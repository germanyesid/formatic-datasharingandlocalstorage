import { Component, OnInit } from '@angular/core';
import { StorageService } from '../storage.service';

@Component({
    selector: 'app-parent',
    template: `
        <h1>Parent</h1>
        <p>Mi mamá manda a decir: {{childrenInfo}}</p>
        <button (click)="changeMessage()">Change Message</button>
        <app-child (childIsYelling)="listenToTheChild($event)"></app-child>
    `,
    styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {
  
    content = "Hello son";
    childrenInfo = "";

    constructor(private storage: StorageService) { }
    
    ngOnInit(){
      let savedData = this.storage.get();
      if(typeof savedData == "string"){
        /*this.storage.save({
          deudor: "Jorge",
          valor: "1.000",
          acreedor: "Pablo"
        });*/
      }else{
        console.log(savedData);
      }
    }

    listenToTheChild(text){
      this.childrenInfo = text;
    }

    changeMessage(){
      this.content = "Vea, nada de falsos positivos";
    }
}